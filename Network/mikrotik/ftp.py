import ftplib
import os
import mtkbkp

ftpsv = "ip-address"
ftport = 21
ftpuser = "Login"
ftpass = "Password"
ftpath = os.path.abspath(mtkbkp.name_arch)
ftpupload=open(str(ftpath), 'rb')

ftp_connect = ftplib.FTP(ftpsv, ftpuser, ftpass)
ftp_connect.login(ftpuser, ftpass)
ftp_connect.encoding = "utf-8"
ftp_connect.set_pasv(False)

ftp_connect.cwd('/FTP/mtkbkp')

ftp_connect.retrlines('LIST')

ftp_connect.storbinary("STOR "+mtkbkp.name_arch,ftpupload)

ftp_connect.quit()
print("Copy archive to ftp-server done!")
