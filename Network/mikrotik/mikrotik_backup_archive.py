import paramiko
import time
import zipfile
import os

time =(time.strftime("%d%m%Y"+"_%H%M%S"))
archive = zipfile.ZipFile(f'{time}'+'.zip', 'w')
file_name = ""


hosts = {
	'ip-address' : 'name',
	'ip-address' : 'name'
		}
for host in hosts:
	hostname = hosts[host]
	file_name = (f'{time}_{hostname}.rsc')
	print('Connecting to', hostname)
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(hostname=host, username='Login', password='Password', port=22, look_for_keys=False, allow_agent=False)
	stdin, stdout, stderr = client.exec_command('/export compact')
	file = open( f'{file_name}', 'w')
	file.write((stdout.read() + stderr.read()).decode('utf-8').replace('\n',''))
	file.close()
	archive.write(f'{file_name}')
	client.close()
	os.remove(f'{file_name}')
	print(f'Backup {hostname} create!')
	print(archive.printdir())
	print(f'Backup {hostname} add archive!')

archive.close()
print('The end!')
