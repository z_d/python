import paramiko
import time

time =(time.strftime("%d%m%Y"+"_%H%M%S"))

hosts = {
	'192.168.2.254' : 'mtk2',
	'192.168.1.254' : 'mtk1'
		}
for host in hosts:
	name = hosts[host]
	print('Connecting to', name)
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(hostname=host, username='login', password='password', port=22, look_for_keys=False, allow_agent=False)
	stdin, stdout, stderr = client.exec_command("/export compact")
	data = stdout.read() + stderr.read()
	content = data.decode('utf-8').replace('\n','')
	file = open( f'{time}_{name}.rsc', 'w')
	file.write(content)
	file.close()
	client.close()
	print(f'Backup {name} create!')
