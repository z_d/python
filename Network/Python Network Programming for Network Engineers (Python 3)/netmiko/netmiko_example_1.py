from netmiko import ConnectHandler

cisco_router = {
    'device_type': 'cisco_ios',
    'ip': '192.168.1.1',
    'username':'admin',
    'password':'password'
}

net_connect = ConnectHandler(**cisco_router)
output = net_connect.send_command('sh run')
print (output)
