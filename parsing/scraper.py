# scraper.py
# https://pythonru.com/biblioteki/parsing-na-python-s-beautiful-soup
import requests
from bs4 import BeautifulSoup     #импорт библиотек

url = 'https://quotes.toscrape.com/'   #ссылка на сайт в переменную
response = requests.get(url)           #делаем запрос с сайта 
soup = BeautifulSoup(response.text, 'lxml')   #записываем вывод в переменую в формате
quotes = soup.find_all('span', class_='text')
authors = soup.find_all('small', class_='author')
tags = soup.find_all('div', class_='tags')   #переменные с результатами парсинга.



for i in range(0, len(quotes)):    # проходим циклом в диапозоне от нуля до значения количества в переменной
    print(quotes[i].text)          # выводим текст
    print('--' + authors[i].text)   #выводим автора
    tagsforquote = tags[i].find_all('a', class_='tag') #переменная с тегами
    for tagforquote in tagsforquote:  #циклом выводим значения в переменной 
        print(tagforquote.text)
    print('\n')  #делаем перенос
